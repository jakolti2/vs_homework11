﻿#include <iostream>

class Animal {
public:
    virtual void Voice() {
        std::cout << "..." << std::endl;
    }
};

class Dog : public Animal {
public:
    void Voice() override {
        std::cout << "Woof!" << std::endl;
    }
};

class Cat : public Animal {
public:
    void Voice() override {
        std::cout << "Meow!" << std::endl;
    }
};

class Cow : public Animal {
public:
    void Voice() override {
        std::cout << "Moooo!" << std::endl;
    }
};

int main() {
    int size = 3;
    auto animals = new Animal * [size] {new Dog, new Cat, new Cow};

    for (auto i = 0; i < size; i++) {
        animals[i]->Voice();
    }

    return 0;
}